====== Evol script commands ======

*chatjoin chatId [, char [,password]]

Force player to join chat with id chatId.

Example:
    chatjoin .chat;

---------------------------------------

*setcamnpc [npcName [, x, y]]

Enable camera moving mode in client and move camera to npc with name "npcName"
and move relative from it in x and y pixels.
If "npcName" is missing, using attached npc.
If x and y missing, it count as 0.

Example:
    setcamnpc 32, 0;
    setcamnpc "npc1";

---------------------------------------

*setcam x, y

Move camera to absolute position x,y in pixels.

Example:
    setcam 0, 0;
    setcam 2000, 512;

---------------------------------------

*movecam x, y

Enable camera moving mode in client and move camera relative to player
position by x and y pixels.

Example:
    movecam 10, 20;

---------------------------------------

*restorecam

Disable camera moving mode in client and set camera to default position.

Example:
    restorecam;

---------------------------------------

*npctalk3 text

Send chat message from current npc, visible only for attached player.
Npc name will be translated.

Example:
    npctalk3 "hello";

---------------------------------------

*closedialog

Will close npc dialog in client. If npc script will continue after this command,
dialog can be opened again.
Player must be attached.

Example:
    closedialog;

---------------------------------------

*clear

Clear from text npc dialog for attached npc.
Player must be attached.

Example:
    clear;

---------------------------------------

*shop npcName

Open if exists for attached player npc shop from npc with name "npcName"

Example:
    shop "bag";

---------------------------------------

*getitemlink itemName [, cardName1 [, cardName2 [, cardName3 [, cardName4]]]]
*getitemlink itemId [, cardName1 [, cardName2 [, cardName3 [, cardName4]]]]

Return link for item id "itemId" or name "itemName" with cards.
This link after can be used in mes or other commands what show messages in npc dialog.

Example:
    mes "Acorn link: " + getitemlink(Acorn);
    mes getitemlink(VneckJumper, MintCashmereDye);

---------------------------------------

*getinvindexlink index

Return link to item by player inventory index. Also return cards in link.
This link after can be used in mes or other commands what show messages in npc dialog.
Inventory index counted from 0.

Example:
    mes "link here: " + getinvindexlink(3);

---------------------------------------

*l text [, param1, ...]

Return translated formatted string.
Player must be attached.

Example:
    mes l("test line");
    mes l("give me @@.", getitemlink(Acorn));

---------------------------------------

*lg femaleText, makeText, [, param1, ...]
*lg text

Return translated formatted string based on gender.
Player must be attached.
Translators will give text with #N.

Example:
    // translators will get two strings "i went to shop.#0" and "i went to shop.#1"
    // with #0 for female and #1 for male
    lg("i went to shop.");
    lg("call her @@.", "call him @@.", "test");

---------------------------------------

*requestlang

Return selected language in client to server.
Player must be attached.

Example:
    .@lang = requestlang();

---------------------------------------

*requestitem

Return selected item id by player. In client player move this item to npc.
Player must be attached.

Example:
    .@item = requestitem();
    mes "You gave me " + getitemlink(.@item);

---------------------------------------

*requestitems [n]

Return n items from player what he moved to npc inventory separated by ";".
If n missing it mean 1.
Better not use this command. Better use requestcraft.
Player must be attached.

Example:
    .@str$ = requestitems();
    .@str$ = requestitems(3);

---------------------------------------

*requestitemindex

Return inventory index for selected item by player. In client player move this item to npc.
Player must be attached.

Example:
    .@item = requestitemindex();

---------------------------------------

*requestitemsindex [n]

Return inventory indexes for n items from player what he moved to npc inventory separated by ";".
If n missing it mean 1.
Player must be attached.

Example:
    .@str$ = requestitemsindex();
    .@str$ = requestitemsindex(3);

---------------------------------------

*requestcraft [n]

Request selected craft items in string format.
n is craft inventory size. If n missing, it mean 1.
After this string must be validated and converted to craft object
Player must be attached.

Example:
    .@str$ = requestcraft(9);

---------------------------------------

*initcraft str

Create craft object based on craft string sent from client.
Return craft object id. Or -1 on error.
After using, craft object must be deleted with function deletecraft.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    deletecraft(.@craft);

---------------------------------------

*dumpcraft id

Dump to server console given craft object with id 'id'.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    dumpcraft(.@craft);
    deletecraft(.@craft);

---------------------------------------

*deletecraft id

Delete craft object.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    deletecraft(.@craft);

---------------------------------------

*findcraftentry id, flag

Search craft entry in craft db for craft object with id 'id' and flag 'flag'.
Retrun craft entry id what was found. If error happend, return -1.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    .@entry = findcraftentry(.@craft, 0);
    deletecraft(.@craft);

---------------------------------------

*usecraft id

This command can be used after checked craft object and found craft entry.
It apply craft entry. May delete or give items and do other things.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    .@entry = findcraftentry(.@craft, 0);
    usecraft .@craft;
    deletecraft(.@craft);

---------------------------------------

*getcraftcode id

After craft object was used by function usecraft, this function based on craft entry id
can return special number configured for craft entry in craft db.
Based on this value script may do additional things.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    .@entry = findcraftentry(.@craft, 0);
    usecraft .@craft;
    .@code = getcraftcode(.@entry);
    deletecraft(.@craft);

---------------------------------------

*getcraftslotid id, slot

Return item id selected in craft object 'id' and in slot 'slot'.
On error return 0.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    .@item = getcraftslotid(.@craft, 0);
    deletecraft(.@craft);

---------------------------------------

*getcraftslotamount id, slot

Return amount selected in craft object 'id' and in slot 'slot'.
On error return 0.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    .@item = getcraftslotid(.@craft, 0);
    .@amount = getcraftslotamount(.@craft, 0);
    deletecraft(.@craft);

---------------------------------------

*validatecraft id

Validate craft object and current player inventory.
It can be used after "next" command between init and use craft.
Return 0 if craft object now invalid, 1 if craft object can be used for player.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    next;
    if (valudatecraft(.@craft) == 0)
    {
        deletecraft(.@craft);
        close;
    }
    .@item = getcraftslotid(.@craft, 1);
    deletecraft(.@craft);

---------------------------------------

*getq quest

Return quest state for attached player.

Example:
    mes getq(ShipQuests_Julia);

---------------------------------------

*setq quest, value

Set quest state for attached player.

Example:
    setq ShipQuests_Julia, 1;

---------------------------------------

*setnpcdir [npc,] dir

Set direction 'dir' for npc with name 'npc' or attached npc.

Example:
    setnpcdir DOWN;
    setnpcdir "npc1", LEFT;

---------------------------------------

*npcsit [name]

Set npc it sit state. 'name' is npc name. If 'name' missing it using attached npc.

Example:
    npcsit;
    npcsit "alige";

---------------------------------------

*npcstand [name]

Set npc it stand state. 'name' is npc name. If 'name' missing it using attached npc.

Example:
    npcstand;
    npcstand "alige";

---------------------------------------

*setnpcsex npcName, gender

Set gender 'gender' for npc 'npcName'
Supported genders:
    G_FEMALE - female
    G_MALE   - male
    G_OTHER  - no gender or other

Example:
    setnpcsex "npc1", G_MALE;

---------------------------------------

*npcwalkto x, y

This command start walking attached npc to position x,y.

Example:
    npcwalkto 10, 10;

---------------------------------------

*setnpcdialogtitle title

Set dialog title for attached npc to 'title'.
Player must be attached.

Example:
    setnpcdialogtitle "Hello";

---------------------------------------

*rif condition, trueValue [, falseValue]

Check condition and if it true, then return 'trueValue' string
If condition false, it return 'falseValue' if present, or empty string.
This command can be used in menu and select commands. Empty lines in this commands hidden.

Example:
    .@ret = select(rif(countitem(Acorn) > 0, "I can give you acorn."),
        "Nothing");

---------------------------------------

*misceffect effect [, name]
*misceffect effect [, id]

Start paritcle effect with id 'effect' on any being with name 'name' or with id 'id'.

Example:
    misceffect 4, getcharid(3);

---------------------------------------

*setmapmask mapName, mask

Allow change map mask. Based on this mask layers in client can be visible or hidden.
Command 'setmapmask' set mask for 'mapName' to value 'mask'.
Default mask is 1.

Example:
    setmapmask "test", 1;  // default
    setmapmask "test", 3;  // 1 + 2

---------------------------------------

*addmapmask mapName, mask

Allow change map mask. Based on this mask layers in client can be visible or hidden.
Command 'addmapmask' add bit mask 'mask' to map 'mapName'.
Default mask is 1.

Example:
    setmapmask "test", 1;  // now mask 1
    addmapmask "test", 2;  // now mask is 1|2 = 3
    addmapmask "test", 2;  // now mask is 3|2 = 3

---------------------------------------

*removemapmask mapName, mask

Allow change map mask. Based on this mask layers in client can be visible or hidden.
Command 'removemapmask' remove bit mask 'mask' from map 'mapName'.
Default mask is 1.

Example:
    setmapmask "test", 1;  // now mask 1
    addmapmask "test", 2;  // now mask is 1|2 = 3
    removemapmask "test", 1;  // now mask is 3|1^1 = 2
    removemapmask "test", 1;  // now mask is 2|1^1 = 2

---------------------------------------

*getmapmask mapName

Return current mask what was set to map 'mapName'.

Example:
    mes "test map mask: " + getmapmask("test");

---------------------------------------

*showavatar [id]

Show avatar in npc dialog for attached npc.
if 'id' misisng or 0 it hide avatar.
Player must be attached.

Example:
    showavatar 1;  // show 1
    showavatar;  // hide

---------------------------------------

*setavatardir dir

Set avatar direction 'dir' in npc dialog for attached npc.
Player must be attached.

Example:
    setavatardir UPRIGHT;

---------------------------------------

*setavataraction action

Set avatar action to 'action' in npc dialog for attached npc.
Player must be attached.

Example:
    setavataraction ACTION_SIT;

---------------------------------------

*changemusic map, file

Change background music on map 'map' to file 'file'.

Example:
    changemusic "music1.ogg";

---------------------------------------

*getmapname

Return attached player current map name.

Example:
    mes "You located in map: " + getmapname();

---------------------------------------

*unequipbyid id

Unequip from attached player item with id 'id'.

Example:
    unequipbyid VneckJumper;

---------------------------------------

*ispcdead

Return true if attached player is dead. In other cases return false.

---------------------------------------

*areatimer mapName, x1, y1, x2, y2, time, eventLabel

Create one time timer in map 'mapName' for players in rectangle (x1,y1) - (x2,y2)
Timer will call label 'eventLabel' after time 'time'.
This function probably will be removed in future.

Example:
    areatimer "test", 10, 10, 20, 20, 5, "npc1:AreaLabel"

---------------------------------------

*getareadropitem mapName, x1, y1, x2, y2, itemId [, delFlag]
*getareadropitem mapName, x1, y1, x2, y2, itemName [, delFlag]

Return number of floor items with id 'itemId' or name 'itemName' in map 'mapName'
in rectangle (x1,y1) - (x2,y2). If delFlag set to 1, it also delete floor items.
This function probably will be removed in future.

Example:
    mes "Acorns amount: " + getareadropitem("test", 10, 10, 20, 20, Acorn);

---------------------------------------

*clientcommand command

Send client side command to client. It allow send only safe commands.
Player must be attached.

Example:
    clientcommand "emote 1";  // show first emote on attached player

---------------------------------------

*isunitwalking [unitId]

Return true if unit with id 'unitId' in walking progress.
If 'unitId' missing it using attached npc.
This function can be used in walking npc scripts.

Example:
    mes "Current npc walking? " + (isunitwalking() ? "yes" : "no");

---------------------------------------

*failedrefindex index

This function do fail refine action on item with inventory index 'index'.
Index start count from 0.
Player must be attached.

Example:
    failedrefindex 3;

---------------------------------------

*downrefindex index, levels

This function lower refine item level with inventory index 'index' for 'levels' levels.
Index start count from 0.
Player must be attached.

Example:
    downrefindex 3, 1;

---------------------------------------

*successrefindex index, levels

This function up refine item level with inventory index 'index' for 'levels' levels.
Index start count from 0.
Player must be attached.

Example:
    successrefindex 3, 1;

---------------------------------------

*isstr var

Return true if 'var' is string, in other case return false.
Can be usefull for check functions arguments.

Example:
    mes "is string is string? " + isstr("text");

---------------------------------------

*setbgteam bgId, teamId;

Set id 'teamId' for battle ground group 'bgId'.
After battle ground start, teamId will be sent to client.

Example:
    $@bgid1 = waitingroom2bg("testbg", 10, 10, "bgnpc1::OnLogout","bgnpc1:OnDie");
    setbgteam $@bgid1, 1;
    bg_warp $@bgid1, "testbg", 10, 10;

---------------------------------------

*checknpccell 

Check given cell in map for flag for attached npc.
Most time it used for check is this cell walkable for npc or not.

Example:
    .@canWalk = checknpccell("test", 10, 10, cell_chkpass);

---------------------------------------

*setcells mapName, x1, y1, x2, y2, mask, wallName

Add to map with name 'mapName' new "wall" in rectangle (x1,y1) - (x2,y2).
Set to this rectangle mask 'mask'. 'wallName' is name for this "wall".
Mask is client side collision types.
After it can be removed by this name.

Example:
    setcells "test", 14, 11, 17, 11, 3, "wall1";

---------------------------------------

*delcells wallName

Remove wall created by command setcells.

Example:
    setcells "test", 14, 11, 17, 11, 3, "wall1";
    delcells "wall1";

---------------------------------------

*setmount id

Set mount (horse) for attached player.
If id is zero, mount removed.

Example:
    setmount 1;

---------------------------------------

*setskin name

Set skin with name 'name' for attached npc for attached player.

Example:
    setskin "test2";

---------------------------------------

*emotion id [, flag [, name]]

This command works same like hercules emotion command, except it removed limit for emotes.

---------------------------------------

successremovecardsindex index

This command remove all cards from item by inventory index and put all cards in inventory.
Also show status effect 3.

---------------------------------------

failedremovecardsindex index, flag

This command remove all cards from item by inventory index.
Also show status effect 2.
It also may remove item or cards depend on flag.
flag value:
    0 - will remove item and cards.
    1 - will remove cards and keep item.
    2 - will remove item and keep cards.
    3 - do nothing except status effect.
